
# Component

## To use the component
```yaml
include:
  - component: gitlab.com/swe-ismoilov/go-component/component@<version>
    inputs:
      stage-build: build
      stage-test: test
      binary: bin/main
      go-version: latest
      main-file: cmd/main.go
```

**stage-build**: stage name for building your go project

**stage-test**: stage name for testing your go project, it includes race testing

**binary**: directory for your binary, which will be stored at, at this destination you can get an artifact of your binary file too

**go-version**: version of your go tool chain

**main-file**: directory or file, that is included main function of your whole project

## You can deploy your project with the component soon!