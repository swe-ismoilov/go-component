package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		_, _ = fmt.Fprintf(c.Writer, "Hello gitlab-ci")
	})
	r.GET("/ping", func(c *gin.Context) {
		_, _ = fmt.Fprintf(c.Writer, "<h1>pong</h1>")
	})

	_ = r.Run(":8007")
}
